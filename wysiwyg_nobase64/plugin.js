(function()
{
	CKEDITOR.plugins.add('wysiwyg_nobase64',
	{
		init: function(editor, pluginPath)
		{
			CKEDITOR.on('instanceReady', function (ev) {
			  ev.editor.on('paste', function (ev) {
			    if (!(typeof ev.data.html === "undefined")) {
			      ev.data.html = ev.data.html.replace(/<img( [^>]*)?>/gi, '');
			    }
			  });    
			
			  ev.editor.document.on('DOMNodeInserted', function (ev) {
			    var target = ev.data.getTarget();
			    if (target.is && target.is('img') && target.getAttribute('src') && target.getAttribute('src').substr(0, 5) === 'data:') {
			      target.remove();
			    }
			  });
			});
		}
	});
})();
